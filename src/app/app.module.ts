import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MisComponentesComponent } from './componentes/mis-componentes/mis-componentes.component';

@NgModule({
  declarations: [
    AppComponent,
    MisComponentesComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
